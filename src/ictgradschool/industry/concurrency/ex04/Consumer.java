package ictgradschool.industry.concurrency.ex04;

import java.util.concurrent.BlockingQueue;

public class Consumer implements Runnable {

    BlockingQueue<Transaction> queue;
    BankAccount account;

    public Consumer(BlockingQueue<Transaction> queue, BankAccount account) {
        this.queue = queue;
        this.account = account;
    }


    @Override
    public void run() {
        Transaction nextT;

        while (!Thread.currentThread().isInterrupted() || queue.size() != 0) {
            try {
                nextT = queue.take();
                SerialBankingApp.doTransaction(nextT, account);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }


//  Didn't have error handling in this one, so copied the above from what Cameron showed to Cher. basically similar though.

//    @Override
//    public void run() {
//        Transaction nextT;
//
//
//        while ( || queue.size()!=0) {
//
//
//            if ((nextT = queue.poll()) != null) {
//                // uncomment this to slow down the processing and see what happens with incorrect join & interrupts
//                //System.out.println("gonna consume");
//                switch (nextT._type) {
//                    case Deposit:
//                        account.deposit(nextT._amountInCents);
//                        break;
//                    case Withdraw:
//                        account.withdraw(nextT._amountInCents);
//                        break;
//
//                }
//            }
//        }
//    }

}
