package ictgradschool.industry.concurrency.ex04;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ConcurrentBankingApp {
    public static void main(String[] args) throws InterruptedException {

        // things that are common between the prod/cons
        BankAccount account = new BankAccount();
        final BlockingQueue<Transaction> queue = new ArrayBlockingQueue<>(10);

        // the runnable for the producer
        Thread producer = new Thread(new Runnable() {
            @Override
            public void run() {
                List<Transaction> transactions = TransactionGenerator.readDataFile();

                // needs to "add these to a BlockingQueue object w/ capacity 10"
                for (Transaction transaction : transactions) {
                    try {
                        queue.put(transaction);
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                }
            }
        });

        // 1. create threads
        Thread[] consumers = new Thread[2];
        for (int i = 0; i < consumers.length; i++) {
            consumers[i] = new Thread(new Consumer(queue, account));
        }

        // 2. start all of the threads
        producer.start();
        for (Thread consumer : consumers) {
            consumer.start();
        }

        // 3. after producer is finished, terminate
        // needs the join to make sure the thing isn't terminated before it's finished running
        producer.join();
        // don't need to interrupt it after, because the join already means it'll be finished before moving on

        //4. interrupt the consumers
        // this 'requests' that they interrupt, so that when they empty the queue they will terminate (and thus the
        // program can finish...)
        for (Thread consumer : consumers) {
            consumer.interrupt();
        }

        //5. wait for consumers to terminate
        // this means that the balance won't be printed until the consumers have actually finished processing all the data
        for (Thread consumer : consumers) {
            consumer.join();
        }

        //6. print the final balance
        System.out.println("Final balance: " + account.getFormattedBalance());


    }

}