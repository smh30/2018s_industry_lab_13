package ictgradschool.industry.concurrency.ex05;

import ictgradschool.Keyboard;

import java.util.List;

public class PrimesProgram {
    public static void main(String[] args) throws InterruptedException {
        PrimesProgram p = new PrimesProgram();
        p.start();
    }

    public void start() throws InterruptedException {
        System.out.println("Value of N to compute prime factors of: ");
        long n = 0;
        while (n == 0) {
            try {
                n = Long.parseLong(Keyboard.readInput());
            } catch (NumberFormatException e) {
                System.out.println("Please enter a valid number: ");
                n = 0;
            }
        }
        // gotta create the PFT with a variable so we can access the methods later?
        PrimeFactorsTask p = new PrimeFactorsTask(n);
        Thread compute = new Thread(p);

        //except apparently the cancellation should be done by its own thread???
        Thread abort = new Thread(new Runnable() {
            @Override
            public void run() {

                System.out.println("To cancel, press q");
                String input = "";
        // this thing never notices that it is interrupted because it never loops unless
                // a key is pressed. how to make it check??
                while ((input = (Keyboard.readInput().trim())).equals("") && !Thread.currentThread().isInterrupted()) {


                    if (input.toLowerCase().equals("q")) {

                        compute.interrupt();
                    } else {
                        input = "";
                    }

                }

            }
        });

// first start the aborter - so that it prints the 'to abort press q' message first rather than at the end
        abort.start();
        // then start the computation
        compute.start();

//do join so that it won't do anything else until the computation is finished
        compute.join();

        abort.interrupt();

        if (p.getState() == PrimeFactorsTask.TaskState.Completed) {
            List<Long> pf = p.getPrimeFactors();

            System.out.println("The prime factors of " + n + " are: ");
            for (Long aLong : pf) {
                System.out.println(aLong);
            }
        }

        // todo issue now is that the program closes fine when i manually quit,
        // but if I don't quit it doesn't end even after the calculation is done
        // until a key is pressed (thus loopiing the abort method so it ends)


    }
}
