package ictgradschool.industry.concurrency.ex05;

import java.util.ArrayList;
import java.util.List;

public class PrimeFactorsTask implements Runnable {
    public long n;
    List<Long> factors;
    public TaskState state;

    public enum TaskState {Initialized, Completed, Aborted}

    ;

    public PrimeFactorsTask(long n) {
        this.n = n;
        factors = new ArrayList<Long>();
        state = TaskState.Initialized;
    }

    @Override
    public void run() {
        while (state == TaskState.Initialized) {
            // for each potential factor
            for (long factor = 2; factor * factor <= n; factor++) {

                if (Thread.currentThread().isInterrupted()) {
                    System.out.println("comp int");
                    state = TaskState.Aborted;
                    return;
                }
                // if factor is a factor of n, repeatedly divide it out
                while (n % factor == 0) {
                    factors.add(factor);
                    n = n / factor;
                }
            }

            // if biggest factor occurs only once, n > 1
            if (n > 1) {
                factors.add(n);
            }
            // when done, set the state to show completed
            state = TaskState.Completed;
            return;
        }
    }


    public long n() {
        return this.n;
    }

    public List<Long> getPrimeFactors() throws IllegalStateException {
        if (state == TaskState.Completed) {
            System.out.println("complete");
            return factors;
        } else {
            System.out.println("not complete");
            throw new IllegalStateException();
        }

    }

    public TaskState getState() {
        return state;
    }
}
