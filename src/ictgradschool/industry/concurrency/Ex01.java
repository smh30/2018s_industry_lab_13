package ictgradschool.industry.concurrency;



public class Ex01 {
    public void start() throws InterruptedException {

        System.out.println("Creating Runnable");

        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                if (Thread.currentThread().isInterrupted()){

                   return;
                }
                for (int i = 0; i < 1000000; i++) {
                    System.out.println(i);
                }
            }
        };

        Thread t = new Thread(myRunnable);
        System.out.println("Starting Runnable");
        t.start();

        t.interrupt();

t.join();
        System.out.println("Ending");




    }

        public static void main (String[]args) throws InterruptedException{
            Ex01 a = new Ex01();
            a.start();
        }


// my original answers down below; as expected, I was way off track
//I'm not sure that I understand what's being asked by this question?
//    public static void main(String[] args) {
//        Ex01 a = new Ex01();
//        a.start();
//    }
//
//    public void start(){
//        t.start();
//
//    }
//
//
//   Thread t = new Thread (new Runnable(){
//        public void run(){
//            for (int i = 0; i < 1000000 ; i++) {
//                System.out.println(i);
//            }
//        }
//    });

}
