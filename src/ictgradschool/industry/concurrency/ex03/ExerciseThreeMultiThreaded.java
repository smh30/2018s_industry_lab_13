package ictgradschool.industry.concurrency.ex03;

import java.util.concurrent.ThreadLocalRandom;

import static java.lang.Math.sqrt;

/**
 * TODO Create a multi-threaded program to calculate PI using the Monte Carlo method.
 */
public class ExerciseThreeMultiThreaded extends ExerciseThreeSingleThreaded {

    /**
     * Estimates PI using a multi-threaded Monte Carlo method.
     */
    @Override
    protected double estimatePI(long numSamples) {

        // this ValueTracker exists because we can't increment the value inside the runnable and then
        // get it back out. but if we call this method and increment a value that's out here,
        // it works ok and we can get the value for our calculation at the end.
        class ValueTracker {
            long numInCircle = 0;

            public long getNumInCircle() {
                return numInCircle;
            }

            public synchronized void incNumInCircle(long numInCircle) {
                this.numInCircle += numInCircle;
            }
        }

        ValueTracker vals = new ValueTracker();

        final int NUM_THREADS = 8;

        // the Runnable contains the actual actions we want each Thread to perform
        Runnable unitOfWork = new Runnable() {
            @Override
            public void run() {
                ThreadLocalRandom tlr = ThreadLocalRandom.current();

                long numInsideCircle = 0;

                long count = numSamples / NUM_THREADS;

                for (long i = 0; i < count; i++) {

                    double x = tlr.nextDouble();
                    double y = tlr.nextDouble();

//                    if (Math.pow(x, 2.0) + Math.pow(y, 2.0) < 1.0) {
//                        numInsideCircle++;
//                    }

                    // from the example written by Cameron:
                    if (x * x + y * y < 1.0) {
                        numInsideCircle++;
                    }

                }

                vals.incNumInCircle(numInsideCircle);
            }
        };


        // create an array with as many spaces as we want Threads
        Thread[] allThreads = new Thread[NUM_THREADS];

        // make a Thread (worker) for each of the spaces which will do the Runnable unitOfWork (task)
        for (int i = 0; i < allThreads.length; i++) {
            allThreads[i] = new Thread(unitOfWork);
        }

        // start all of the Threads (workers) working on the task
        for (Thread thread : allThreads) {
            thread.start();
        }

        // make it so the rest of the method won't happen until all the the Threads are finished using .join()
        for (Thread thread : allThreads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


        return 4.0 * (double) vals.getNumInCircle() / (double) (numSamples / NUM_THREADS * NUM_THREADS);
    }


    /**
     * Program entry point.
     */
    public static void main(String[] args) {
        new ExerciseThreeMultiThreaded().start();
    }
}
